/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.56.10
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 192.168.56.10:3306
 Source Schema         : gulimall-ums

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 17/12/2020 18:23:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ums_member_level
-- ----------------------------
DROP TABLE IF EXISTS `ums_member_level`;
CREATE TABLE `ums_member_level`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '?ȼ????',
  `growth_point` int(11) NULL DEFAULT NULL COMMENT '?ȼ???Ҫ?ĳɳ?ֵ',
  `default_status` tinyint(4) NULL DEFAULT NULL COMMENT '?Ƿ?ΪĬ?ϵȼ?[0->???ǣ?1->??]',
  `free_freight_point` decimal(18, 4) NULL DEFAULT NULL COMMENT '???˷ѱ?׼',
  `comment_growth_point` int(11) NULL DEFAULT NULL COMMENT 'ÿ?????ۻ?ȡ?ĳɳ?ֵ',
  `priviledge_free_freight` tinyint(4) NULL DEFAULT NULL COMMENT '?Ƿ?????????Ȩ',
  `priviledge_member_price` tinyint(4) NULL DEFAULT NULL COMMENT '?Ƿ??л?Ա?۸???Ȩ',
  `priviledge_birthday` tinyint(4) NULL DEFAULT NULL COMMENT '?Ƿ?????????Ȩ',
  `note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '??ע',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '??Ա?ȼ?' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ums_member_level
-- ----------------------------
INSERT INTO `ums_member_level` VALUES (1, '普通会员', 0, 1, 299.0000, 10, 0, 0, 1, '初级会员');
INSERT INTO `ums_member_level` VALUES (2, '铜牌会员', 3000, 0, 279.0000, 30, 0, 1, 1, '铜牌会员');
INSERT INTO `ums_member_level` VALUES (3, '银牌会员', 5000, 0, 229.0000, 50, 0, 1, 1, '银牌会员');

SET FOREIGN_KEY_CHECKS = 1;
