package com.atguigu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithList() {
        List<CategoryEntity> entities = baseMapper.selectList(null);

        List<CategoryEntity> level1Menus = entities.stream().filter(category ->
                category.getParentCid() == 0
        ).map(menu -> {
            menu.setChildren(getChildrens(menu, entities));
            return menu;
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());


        return level1Menus;
    }

    @Override
    public void removeMenusByIds(List<Long> asList) {
        //TODO 判断引用
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findCatelogPath(Long catelogId) {

        List<Long> path = new ArrayList<>();
        List<Long> prantPath = findPrantPath(catelogId, path);

        Collections.reverse(prantPath);
        return prantPath.toArray(new Long[prantPath.size()]);
    }

    /**
     * 级联更新
     *
     * @param category
     */
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
    }

    @Cacheable(value = {"category"},key = "'level1Categorys'")
    @Override
    public List<CategoryEntity> getCategoryLevel1() {

        List<CategoryEntity> categoryEntities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", 0));
        return categoryEntities;
    }

    //TODO 堆外内存溢出 原因是boot2.0以后操作redis 使用的是lettuce 没有好的办法   可以选择更换jedis 缺点很久没更新
    @Override
    public Map<String, List<Catelog2Vo>> getCatelogJson() {

        String catelogJSON = (String) redisTemplate.opsForValue().get("catelogJSON");
        if (StringUtils.isEmpty(catelogJSON)) {
            // Map<String, List<Catelog2Vo>> json = getCatelogJsonForDB();
            Map<String, List<Catelog2Vo>> json = getCatelogJsonForRedisLock();
            return json;
        }
        //json转对象
        Map<String, List<Catelog2Vo>> result = JSON.parseObject(catelogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
        });

        return result;
    }

    public Map<String, List<Catelog2Vo>> getCatelogJsonForRedisLock() {

        String uuid = UUID.randomUUID().toString();
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);
        if (lock) {
            Map<String, List<Catelog2Vo>> dateFromDb;
            try {
                dateFromDb = getCatelogJsonForDB();
            } finally {
                //设置解锁 使用redis代码段 存在就解锁

                String srcipt = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";

                Long lcock = redisTemplate.execute(new DefaultRedisScript<Long>(srcipt, Long.class), Arrays.asList("lcock"), uuid);
            }

            return dateFromDb;
        } else {
            try {
                Thread.sleep(200);
            } catch (Exception e) {
                log.error("加锁睡眠异常");
            }

            return getCatelogJsonForRedisLock();
        }


    }


    public Map<String, List<Catelog2Vo>> getCatelogJsonForDB() {
        String catelogJSON = (String) redisTemplate.opsForValue().get("catelogJSON");
        if (!StringUtils.isEmpty(catelogJSON)) {
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(catelogJSON, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });

            return result;
        }

        //TODO  实际情况中影该是分布式锁
        //synchronized (this) { // 本地加锁 锁住的是单体应用
        return getDateFromDb();
        // }


    }

    private Map<String, List<Catelog2Vo>> getDateFromDb() {
        List<CategoryEntity> categoryLevel1 = getCategoryLevel1();
        List<CategoryEntity> selectList = baseMapper.selectList(null);


        Map<String, List<Catelog2Vo>> listMap = categoryLevel1.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            List<CategoryEntity> category2 = getParentCid(selectList, v.getCatId());
            //baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId())); 查看
            List<Catelog2Vo> catelog2Vos = null;
            if (category2 != null) {
                catelog2Vos = category2.stream().map(item -> {

                    //查找三级分类
                    List<CategoryEntity> catrlog3 = getParentCid(selectList, item.getCatId());
                    // baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", item.getCatId()));
                    List<Catelog2Vo.Catelog3Vo> catrlog3Level = null;
                    if (catrlog3 != null) {
                        catrlog3Level = catrlog3.stream().map(i -> {
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(item.getCatId().toString(), i.getCatId().toString(), i.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                    }

                    Catelog2Vo catelog2Vo = new Catelog2Vo(item.getParentCid().toString(), catrlog3Level, item.getCatId().toString(), item.getName());
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2Vos;
        }));
        //转换json
        String s = JSON.toJSONString(listMap);
        redisTemplate.opsForValue().set("catelogJSON", s);
        return listMap;
    }

    private List<CategoryEntity> getParentCid(List<CategoryEntity> selectList, Long catId) {

        List<CategoryEntity> collect = selectList.stream().filter(item -> item.getParentCid() == catId).collect(Collectors.toList());

        return collect;
    }

    private List<Long> findPrantPath(Long catelogId, List<Long> path) {

        path.add(catelogId);
        CategoryEntity catrgory = this.getById(catelogId);
        if (catrgory.getParentCid() != 0) {
            findPrantPath(catrgory.getParentCid(), path);
        }

        return path;
    }


    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> children = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid().equals(root.getCatId());
        }).map(categoryEntity -> {
            categoryEntity.setChildren(getChildrens(categoryEntity, all));
            return categoryEntity;
        }).sorted((meun1, menu2) -> {
            return (meun1.getSort() == null ? 0 : meun1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return children;
    }


}