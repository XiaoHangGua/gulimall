package com.atguigu.gulimall.product.web;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    @Autowired
    CategoryService categoryService;

    /**
     * 获取1胡分类数据
     *
     * @param model
     * @return
     */
    @GetMapping({"/", "/index.html"})
    public String index(Model model) {
        //查询所有的1及分类
        List<CategoryEntity> categoryEntities = categoryService.getCategoryLevel1();
        model.addAttribute("categoryLevel1", categoryEntities);
        return "index";
    }

    /**
     * index/catalog.json
     */
    @ResponseBody
    @GetMapping("/index/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatelogIson(Model model) {
        //查询所有的1及分类
        Map<String, List<Catelog2Vo>> map = categoryService.getCatelogJson();
        return map;
    }


}
