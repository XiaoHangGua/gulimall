package com.atguigu.gulimall.product.vo;

import lombok.Data;

@Data
public class AttrRespVO extends AttrVO {

    private String catelogName; // "手机/数码/手机", //所属分类名字
    private String groupName; // "主体", //所属分组名字
    private Long[] catelogPath;// [2, 34, 225] //分类完整路径

}
