package com.atguigu.gulimall.product.exception;

import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@Slf4j

@RestControllerAdvice(basePackages = "com.atguigu.gulimall.product.controller")
public class GulimallExceptionControllerAdvice {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleVaildException(MethodArgumentNotValidException e){
        log.info("数据校验出现问题：{}，异常类型：{}",e.getMessage(),e.getClass());
        BindingResult bindingResult = e.getBindingResult();
        Map<String,Object> resultMap= new HashMap<>();
        bindingResult.getFieldErrors().forEach(item->{
            String field = item.getField();
            String msg = item.getDefaultMessage();
            resultMap.put(field,msg);
        });
        return R.error(400,"数据校验问题").put("data",resultMap);
    }

//    @ExceptionHandler(value = Throwable.class)
//    public R handAllException(Throwable e){
//
//        return R.error();
//    }

}
