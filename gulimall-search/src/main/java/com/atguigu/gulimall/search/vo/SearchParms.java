package com.atguigu.gulimall.search.vo;

import lombok.Data;

import java.util.List;

@Data
public class SearchParms {

    private String keyword; //全文关键字检索
    private Long catalog3Id; //分类id3级
    private String sort; // 所有排序
    private Integer hasStock; //是否有货
    private String skuPrice; // 价格区间
    private List<Long> brand; //品牌 多选
    private List<String> attrs;//属性多选
    private Integer pageNum;
}
