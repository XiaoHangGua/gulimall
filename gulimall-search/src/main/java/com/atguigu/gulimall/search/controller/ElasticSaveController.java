package com.atguigu.gulimall.search.controller;

import com.atguigu.common.exception.BizCodeException;
import com.atguigu.common.to.es.SkuEsModel;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequestMapping("/search/save")
@RestController
public class ElasticSaveController {

    @Autowired
    private ProductSaveService productSaveService;

    @PostMapping("/product")
    public R productStatusUp(@RequestBody List<SkuEsModel> esModels) {
        boolean b = false;
        try {
            b = productSaveService.productStatusUp(esModels);
        } catch (Exception e) {
            log.error("ES商品上架错误", e);
            return R.error(BizCodeException.PRODUCT_UP_VAILD_EXCEPTION.getCode(), BizCodeException.PRODUCT_UP_VAILD_EXCEPTION.getMsg());
        }

        if (!b) {
            return R.ok();
        } else {
            return R.error(BizCodeException.PRODUCT_UP_VAILD_EXCEPTION.getCode(), BizCodeException.PRODUCT_UP_VAILD_EXCEPTION.getMsg());
        }

    }
}
