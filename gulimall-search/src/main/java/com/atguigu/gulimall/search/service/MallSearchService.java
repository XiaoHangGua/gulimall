package com.atguigu.gulimall.search.service;

import com.atguigu.gulimall.search.vo.SearchParms;
import com.atguigu.gulimall.search.vo.SearchResult;

public interface MallSearchService {

    SearchResult search(SearchParms params);
}
