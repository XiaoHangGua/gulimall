package com.atguigu.gulimall.search.vo;

import com.atguigu.common.to.es.SkuEsModel;
import lombok.Data;

import java.util.List;

@Data
public class SearchResult {

    private List<SkuEsModel> products;

    /**
     * 分页信息
     */
    private Integer pageNum;
    private Long total;
    private Integer totalNum;

    /**
     * 涉及到信息
     */
    private List<BrandVo> brands;//所有品牌
    private List<AttrVo> attrs;//所有属性
    private List<CatalogVo> catalogs;//所有分类



    //==============上面返回页面所有信息========
    /**
     * 品牌
     */
    @Data
    public static class BrandVo {
        private Long brandId;

        private String brandName;

        private String brandImg;
    }

    /**
     * 分类
     */
    @Data
    public static class CatalogVo {
        private Long catalogId;

        private String catalogName;

    }

    /**
     * 属性
     */
    @Data
    public static class AttrVo {
        private Long attdId;

        private String attrName;

        private List<String> attrValue;
    }


}
