package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author hwc
 * @email hwc@gmail.com
 * @date 2020-05-26 08:16:57
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
