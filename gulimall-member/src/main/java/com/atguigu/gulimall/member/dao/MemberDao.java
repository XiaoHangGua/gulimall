package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author hwc
 * @email hwc@gmail.com
 * @date 2020-05-26 08:10:40
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
