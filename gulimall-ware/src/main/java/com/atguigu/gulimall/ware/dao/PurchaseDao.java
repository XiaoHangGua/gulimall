package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author hwc
 * @email hwc@gmail.com
 * @date 2020-12-21 17:16:18
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
